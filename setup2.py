import setuptools
import os

if __name__ == '__main__':
    with open("README.md", "r") as fh:
        long_description = fh.read()

    # We now define the ParetoLib version number in ParetoLib/__init__.py
    __version__ = 'unknown'
    for line in open('BoolLearnLib/__init__.py'):
        if (line.startswith('__version__')):
            exec (line.strip('. '))

    setuptools.setup(
        name="BoolLearnLib",
        version=__version__,
        author="J. Ignacio Requeno",
        author_email='irini-ileftheria.iens@univ-grenoble-alpes.fr, jose-ignacio.requeno-jarabo@univ-grenoble-alpes.fr',
        description='BoolLearnLib is a free multidimensional boundary learning library for ' \
                    'Python 2.7, 3.4 or newer',
        long_description=long_description,
        long_description_content_type="text/markdown",
        url='https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/tempo/boolean',
        install_requires=[
            'matplotlib>=1.4.2',
            'numpy>=1.8.2',
            'pytest>=2.0',
            'sortedcontainers>=1.5.10'
        ],
        packages=setuptools.find_packages(),
        classifiers=(
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3.4",
            "License :: GNU GPL",
            "Operating System :: OS Independent",
        ),
        use_2to3=True,
        test_suite=os.path.dirname(__file__) + '.Tests',
        #convert_2to3_doctests=['src/your/module/README.txt'],
        #use_2to3_fixers=['your.fixers'],
        #use_2to3_exclude_fixers=['lib2to3.fixes.fix_import'],
        #license='GPL',
    )
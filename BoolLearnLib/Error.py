# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:23:36 2017
last update on Wed 6 Jun 18

@author: elefther, nrequeno
"""

import matplotlib.pyplot as plt
from multiprocessing import Pool, cpu_count
import copy

from BoolLearnLib.Terms import inclusion_dnf, semantics
from BoolLearnLib.Samples import sample_from_DNF_u, random_vector
from BoolLearnLib._py3k import range

# ==============================================================================
# Counting the error
# ==============================================================================

def false_neg(target_dnf, hypothesis_dnf, num_tests=100):
    """on a sample taken from the hypothesis, returns the number of vectors that do not belong to the target, i.e., false positives.
    """
    fals_neg = 0

    for test_vector in sample_from_DNF_u(num_tests, *target_dnf):
        if not inclusion_dnf(test_vector, *hypothesis_dnf):
            fals_neg += 1

    assert num_tests != 0

    return float(fals_neg) / num_tests


def false_pos(target_dnf, hypothesis_dnf, num_tests=100):
    """on a sample taken from the target, returns the number of vectors that do not belong to the hypothesis, i.e., false negatives.
    """
    fals_pos = 0

    for i in range(num_tests):
        test_vector = sample_from_DNF_u(1, *hypothesis_dnf)[0]
        if not inclusion_dnf(test_vector, *target_dnf):
            fals_pos += 1

    return float(fals_pos) / num_tests


def false_neg2(testing_sample, hypothesis_dnf):
    """on a given testing sample, check what is the percentage of vectors NOT belonging to the hypothesis.
    """
    fals_neg = 0

    for test_vector in testing_sample:
        if not inclusion_dnf(test_vector, *hypothesis_dnf):
            fals_neg += 1

    return float(fals_neg) / len(testing_sample)


def false_pos2(testing_sample, hypothesis_dnf):
    """on a given testing sample, check what is the percentage of vectors belonging to the hypothesis.
    """
    fals_pos = 0

    for test_vector in testing_sample:
        if inclusion_dnf(test_vector, *hypothesis_dnf):
            fals_pos += 1

    return float(fals_pos) / len(testing_sample)


# ==============================================================================

# check by counting : find and compare the semantics
def exact_difference(dnf1, dnf2):
    """returns the normalized volume of the symmetric difference"""
    n = len(dnf1[0])
    assert len(dnf2[0]) == n
    sem1 = semantics(*dnf1)
    sem2 = semantics(*dnf2)
    diff = sem1.symmetric_difference(sem2)
    return len(diff) / float(2 ** n)


# ==============================================================================

def approx_difference(target_dnf, hypothesis_dnf, num_tests=100):
    """compute the distance between two DNFs.
    DNF1 stands for the target and DNF2 for the hypothesis.
    """
    n = len(target_dnf[0])  # the dimension
    assert len(hypothesis_dnf[0]) == n

    fals_neg = 0
    fals_pos = 0

    for i in range(num_tests):
        test_vector = random_vector(n)

        test1 = inclusion_dnf(test_vector, *target_dnf)
        test2 = inclusion_dnf(test_vector, *hypothesis_dnf)

        if test1 and not test2:
            fals_neg += 1
        elif not test1 and test2:
            fals_pos += 1

    return (fals_neg + fals_pos) / float(num_tests)


def approx_difference2(target_dnf, hypothesis_dnf):
    """compute the distance between two DNFs.
    DNF1 stands for the target and DNF2 for the hypothesis.
    """
    n = len(target_dnf[0])  # the dimension
    assert len(hypothesis_dnf[0]) == n

    num_tests = 1000

    fals_neg = 0
    fals_pos = 0

    for i in range(num_tests):
        test_vector = random_vector(n)

        test1 = inclusion_dnf(test_vector, *target_dnf)
        test2 = inclusion_dnf(test_vector, *hypothesis_dnf)

        if test1 and not test2:
            fals_neg += 1
        elif not test1 and test2:
            fals_pos += 1

    return (fals_neg + fals_pos) / float(num_tests)


# ==============================================================================

def par_inclusion_dnf(args):
    i, h = args
    return inclusion_dnf(i, *h)


def par_error(hypothesis, testing_sample):
    """the error is given as an approximation of
    the false negatives of the hypothesis,
    given a testing sample of positive examples

        p(h'|f)

    """
    a = len(testing_sample)  # |S1|
    #
    # Start multiprocessing
    p = Pool(processes=cpu_count())
    args_pool = ((i, copy.deepcopy(hypothesis)) for i in testing_sample)
    c_list = list(p.imap_unordered(par_inclusion_dnf, args_pool))
    c = c_list.count(True)
    # Stop multiprocessing
    p.close()
    p.join()
    #

    # c = len([i for i in testing_sample if inclusion_dnf(i, *hypothesis)])  # |S1 \cap h|
    # c = sum(1 for i in testing_sample if inclusion_dnf(i, *hypothesis))  # |S1 \cap h|

    assert a != 0

    # p(h'|f) = The probability of a vector to be outside h, given that it is in f
    return float(a - c) / a


def error(hypothesis, testing_sample):
    """the error is given as an approximation of
    the false negatives of the hypothesis,
    given a testing sample of positive examples

        p(h'|f)

    """
    a = len(testing_sample)  # |S1|
    c = len([i for i in testing_sample if inclusion_dnf(i, *hypothesis)])  # |S1 \cap h|
    # c = sum(1 for i in testing_sample if inclusion_dnf(i, *hypothesis))  # |S1 \cap h|

    assert a != 0

    # print("a " + str(a))
    # print("c " + str(c))

    # p(h'|f) = The probability of a vector to be outside h, given that it is in f
    return float(a - c) / a


def error2(hypothesis, testing_sample_neg):
    """the error is given as an approximation of
    the false positives of the hypothesis,
    given a testing sample of negative examples

        p(h|f')

    """
    a = len(testing_sample_neg)  # |S1|
    c = len([i for i in testing_sample_neg if inclusion_dnf(i, *hypothesis)])  # |S1 \cap h|

    assert a != 0

    # p(h'|f) = The probability of a vector to be outside h, given that it is in f
    return float(c) / a


# ==============================================================================
# Plotting functions
# ==============================================================================

def plot_2D(xlist, ylist, legend=list(), xlabel='x', ylabel='y', title='title', xscale='linear', yscale='linear'):
    # type: (list, list, list, str, str, str) -> plt
    # (name, hex)
    # colorlist = [name for (name, _) in matplotlib.colors.cnames.iteritems()]
    # colorlist = [hex for (_, hex) in matplotlib.colors.cnames.iteritems()]
    # colorlist.sort()

    scale = ['linear', 'log', 'symlog', 'logit']
    assert xscale in scale
    assert yscale in scale

    fig = plt.figure()

    # axes = fig.add_axes([min(xlist), min(ylist), max(xlist), max(ylist)])  # left, bottom, width, height (range 0 to 1)
    axes = fig.add_subplot(111, aspect='equal')

    axes.set_xlabel(xlabel)
    axes.set_ylabel(ylabel)
    axes.set_title(title)

    #
    plt.xlim(xlist[0], xlist[-1])  # range of x-axe
    plt.ylim(ylist[0], ylist[-1])  # range of y-axe
    #

    # plt.plot(xlist, ylist, color=colorlist[0])
    plt.plot(xlist, ylist, linewidth=2.0)

    # Legend
    if len(legend) > 0:
        plt.legend(legend, loc=0)

    # plt.autoscale()
    plt.xscale(xscale)
    plt.yscale(yscale)

    plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25, wspace=0.35)

    plt.show(block=True)
    # plt.close()
    return plt


def plot_list_2D(list_zip_x_y, legend=list(), xlabel='x', ylabel='y', title='title', xscale='linear',
                 yscale='linear'):
    # type: (list, list, str, str, str) -> plt
    # (name, hex)
    # colorlist = [name for (name, _) in matplotlib.colors.cnames.iteritems()]
    # colorlist = [hex for (_, hex) in matplotlib.colors.cnames.iteritems()]
    # colorlist.sort()

    scale = ['linear', 'log', 'symlog', 'logit']
    assert xscale in scale
    assert yscale in scale

    fig = plt.figure()

    # axes = fig.add_axes([min(xlist), min(ylist), max(xlist), max(ylist)])  # left, bottom, width, height (range 0 to 1)
    # axes = fig.add_subplot(111, aspect='equal')
    axes = fig.add_subplot(111)

    axes.set_xlabel(xlabel)
    axes.set_ylabel(ylabel)
    axes.set_title(title)

    # minx = min(xlist[0] for (xlist, _) in list_zip_x_y)
    # maxx = max(xlist[-1] for (xlist, _) in list_zip_x_y)
    # miny = min(ylist[0] for (_, ylist) in list_zip_x_y)
    # maxy = max(ylist[-1] for (_, ylist) in list_zip_x_y)

    minx = min(min(xlist) for (xlist, _) in list_zip_x_y)
    maxx = max(max(xlist) for (xlist, _) in list_zip_x_y)
    miny = min(min(ylist) for (_, ylist) in list_zip_x_y)
    maxy = max(max(ylist) for (_, ylist) in list_zip_x_y)

    #
    plt.xlim(minx, maxx)  # range of x-axe
    plt.ylim(miny, maxy)  # range of y-axe
    #

    # list_zip_x_y = [(x1,y1),(x2,y2),...,(xn,yn)]
    # where:
    # - xi is a list of points in the xaxe
    # - yi is a list of points in the yaxe

    for i, zip_x_y in enumerate(list_zip_x_y):
        xlist, ylist = zip_x_y

        # ind = i % len(colorlist)
        # plt.plot(xlist, ylist, color = colorlist[ind])
        plt.plot(xlist, ylist, linewidth=2.0)

    # Legend
    if len(legend) > 0:
        plt.legend(legend, loc=0)

    # plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.05, wspace=0.05)
    fig.tight_layout()
    plt.tight_layout()

    # plt.autoscale()
    plt.xscale(xscale)
    plt.yscale(yscale)

    plt.show(block=True)
    # plt.close()
    return plt

import unittest

from BoolLearnLib.Table import Table
from BoolLearnLib.Samples import sample_from_rectangle


#########
# Table #
#########

class TermsTestCase(unittest.TestCase):
    def setUp(self):
        self.s = '11*0**'
        self.sample = sample_from_rectangle(self.s, 8)
        self.training_sample = self.sample[1:5]
        self.testing_sample = self.sample[5:8]

    # def test_init(self):

    def test_klearn(self):
        print('A sample of size 5 is used to learn a rectangle (1-term DNF)')
        self.T = Table(self.training_sample, cost_function='dist')

        # The learning function stops when 'h' reaches k = 1 terms
        self.T.klearn(k=1, select='f', print_on=False, print_latex=False)
        # self.assertEqual(self.T.terms, ['1100**'])
        self.assertEqual(self.T.terms, ['11*0**'])

    def test_elearn(self):
        print('A sample of size 5 is used to learn a rectangle (1-term DNF)')
        self.T = Table(self.training_sample, cost_function='dist')

        # The learning function stops when 'h' the number of falses positives using the testing_sample is below an error 'e'
        self.T.elearn(testing_sample=self.testing_sample, e=0.1, select='f', print_on=False, print_latex=False)
        # self.assertEqual(self.T.terms, ['11001*'])
        self.assertEqual(self.T.terms, ['11*0**'])

    def test_dlearn(self):
        print('A sample of size 5 is used to learn a rectangle (1-term DNF)')
        self.T = Table(self.training_sample, cost_function='dist')

        # The learning function stops when 'h' reaches k = 1 terms
        self.T.dlearn(threshold=float('inf'), select='f', print_on=False, print_latex=False)
        # self.assertEqual(self.T.terms, ['1100**'])
        self.assertEqual(self.T.terms, ['11*0**'])


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    unittest.main(testRunner=runner)

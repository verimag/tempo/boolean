#!/usr/bin/env bash
coverage run -m --parallel-mode pytest test_Error.py
coverage run -m --parallel-mode pytest test_Samples.py
coverage run -m --parallel-mode pytest test_Table.py
coverage run -m --parallel-mode pytest test_Terms.py
coverage combine
#coverage report
coverage report --omit=*BoolLearnLib/_py3k*
#coverage html -d coverage/report-python
coverage html --omit=*BoolLearnLib/_py3k* -d coverage/
coverage erase
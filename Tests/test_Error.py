import unittest

from BoolLearnLib.Error import false_neg, false_neg2, false_pos, false_pos2, exact_difference, approx_difference
from BoolLearnLib.Terms import Term
from BoolLearnLib.Samples import sample_from_DNF


#########
# Error #
#########

class ErrorTestCase(unittest.TestCase):
    def setUp(self):
        self.target = Term('1*011**')
        self.hypoth_1 = Term('1*011**')
        self.hypoth_2 = Term('11**1**')

        self.sample = sample_from_DNF(8, self.target.vector)
        self.training_sample = self.sample[1:5]
        self.testing_sample = self.sample[5:8]

    def test_false_neg(self):
        fn1 = false_neg([self.target.vector], [self.hypoth_1.vector], num_tests=8)
        fn2 = false_neg([self.target.vector], [self.hypoth_2.vector], num_tests=8)

        self.assertLessEqual(fn1, fn2)

        fn1 = false_neg2(self.testing_sample, [self.hypoth_1.vector])
        fn2 = false_neg2(self.testing_sample, [self.hypoth_2.vector])

        self.assertLessEqual(fn1, fn2)

    def test_false_pos(self):
        fp1 = false_pos([self.target.vector], [self.hypoth_1.vector], num_tests=8)
        fp2 = false_pos([self.target.vector], [self.hypoth_2.vector], num_tests=8)

        self.assertLessEqual(fp1, fp2)

        fp1 = false_pos2(self.testing_sample, [self.hypoth_1.vector])
        fp2 = false_pos2(self.testing_sample, [self.hypoth_2.vector])

        self.assertLessEqual(fp1, fp2)

    def test_difference(self):
        ed1 = exact_difference([self.target.vector], [self.hypoth_1.vector])
        ed2 = exact_difference([self.target.vector], [self.hypoth_2.vector])

        self.assertLessEqual(ed1, ed2)

        ad1 = approx_difference([self.target.vector], [self.hypoth_1.vector])
        ad2 = approx_difference([self.target.vector], [self.hypoth_2.vector])

        self.assertLessEqual(ad1, ad2)


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    unittest.main(testRunner=runner)

import sys
import argparse
import time
from random import shuffle
from math import ceil, floor
from multiprocessing import cpu_count, Pool

from BoolLearnLib.Table import Table
from BoolLearnLib.Error import plot_list_2D
from BoolLearnLib.Samples import random_DNF, sample_from_DNF_u


def read_DNF_from_file(filename):
    # type: (str) -> list
    # filename = 'C:/path/numbers.txt'
    # Each line of the file contains a kterm of the DNF
    with open(filename) as f:
        DNF = f.read().splitlines()
    return DNF


def par_learning(args):
    # type: (list) -> list
    training_sample, testing_sample, kterm, threshold, e = args

    # Initialize the learning function
    T = Table(training_sample, cost_function='dist')

    T.klearn(k=kterm, select='f', print_on=False, print_latex=False)
    return T.terms


def par_learning2(args):
    # type: (list) -> list
    training_sample, testing_sample, kterm, threshold, e = args

    # Initialize the learning function
    T = Table(training_sample, cost_function='dist')

    T.kdelearn(testing_sample=testing_sample, k=kterm, threshold=threshold, e=e, select='f', print_on=False,
               print_latex=False)
    return T.terms


def parallel_learning2(S, M, k0, k, threshold, e):
    # type: (list, int, int, int, float, float) -> (list, list, list, Table)

    # Multiprocessing
    num_proc = cpu_count()
    p = Pool(num_proc)

    # 1/3 of the sampling goes to training
    training_sample = S[: int(ceil(len(S) / 3.0))]

    # 2/3 of the sampling goes to testing
    # testing_sample = S[1 + int(floor(len(S) / 3.0)):len(S)]
    testing_sample = S[1 + int(floor(len(S) / 3.0)):]

    # diferenceset = (set(testing_sample) - set(training_sample)).union(set(training_sample) - set(testing_sample))
    # print('diferenceset ' + str(len(diferenceset)))

    while M < len(training_sample):
        num_blocks = int(ceil(len(training_sample) / float(M)))
        print('num_blocks ' + str(num_blocks))

        offset = int(ceil(len(training_sample) / num_blocks))
        print('offset ' + str(offset))
        training_sample_par = [training_sample[i * offset: (i + 1) * offset] for i in range(num_blocks)]
        # Remove empty lists
        training_sample_par = [x for x in training_sample_par if x != []]

        offset = int(ceil(len(testing_sample) / num_proc))
        print('offset ' + str(offset))
        testing_sample_par = [testing_sample[i * offset: (i + 1) * offset] for i in range(num_blocks)]
        # Remove empty lists
        testing_sample_par = [x for x in testing_sample_par if x != []]

        args_par_learning = ((train_i, test_i, k0, threshold, e) for train_i, test_i in
                             zip(training_sample_par, testing_sample_par))
        list_of_lists_with_kterms = p.imap_unordered(par_learning, args_par_learning)
        # kterm_list = p.map(par_learning, args_par_learning)
        training_sample = list({kterm for sublist_kterms in list_of_lists_with_kterms for kterm in sublist_kterms})

        print('training_sample ' + str(len(training_sample)))

    # Stop multiprocessing
    p.close()
    p.join()

    # Last iteration
    x = []
    y = []
    z = []

    # Initialize the learning function
    # T = Table(training_sample, cost_function='dist')
    T = Table(training_sample, cost_function='cost')
    # x, y, z = T.kdelearn(testing_sample=testing_sample, k=k, threshold=threshold, e=e, select='f',
    #                      print_on=False, print_latex=False)
    x, y = T.klearn(k=k, select='f', print_on=False, print_latex=False)

    # Length of the training_sample at the final iteration of the learning process
    pad = len(training_sample)
    x = [xi + pad for xi in x]

    # Result
    result = sorted(T.terms)
    expected = sorted(DNF)
    diff = (set(result) - set(expected)) | (set(expected) - set(result))

    print('Result: {0}'.format(str(result)))
    print('Expected: {0}'.format(str(expected)))
    print('Difference: {0}'.format(str(diff)))
    # print('Cost: {0}'.format(str(T.cost)))

    # print('x: {0}'.format(str(x)))
    # print('y: {0}'.format(str(y)))
    # print('z: {0}'.format(str(z)))
    # time.sleep(5)

    return x, y, z, T


def parallel_learning(initial_training_sample, testing_sample, M, k0, k, threshold, e):
    # type: (list, list, int, int, int, float, float) -> (list, list, list, Table)

    # Multiprocessing
    num_proc = cpu_count()
    p = Pool(num_proc)

    # diferenceset = (set(testing_sample) - set(training_sample)).union(set(training_sample) - set(testing_sample))
    # print('diferenceset ' + str(len(diferenceset)))

    print('Configurating parallelism...')
    training_sample = initial_training_sample
    while M < len(training_sample):
        # Dividing the training set in chunks of maximum M elements
        num_blocks = int(ceil(len(training_sample) / float(M)))
        print('num_blocks: ' + str(num_blocks))

        offset = int(ceil(len(training_sample) / num_blocks))
        print('offset training set: ' + str(offset))
        training_sample_par = [training_sample[i * offset: (i + 1) * offset] for i in range(num_blocks)]
        # Remove empty lists that could appear because of ceiling the integer 'offset'
        training_sample_par = [train_i for train_i in training_sample_par if train_i != []]

        # Dividing the testing set in chunks according to the number of cores (i.e., number of concurrent processes)
        # offset = int(ceil(len(testing_sample) / num_proc))
        offset = int(ceil(len(testing_sample) / num_blocks))
        print('offset testing set: ' + str(offset))
        testing_sample_par = [testing_sample[i * offset: (i + 1) * offset] for i in range(num_blocks)]
        # Remove empty lists that could appear because of ceiling the integer 'offset'
        testing_sample_par = [test_i for test_i in testing_sample_par if test_i != []]

        assert len(training_sample_par) == len(testing_sample_par), 'Each training set must have a testing set'
        assert len(training_sample_par) > 0, 'At least one training set is required'

        # Running the learning of boolean function for each chunk
        args_par_learning = [(train_i, test_i, k0, threshold, e) for train_i, test_i in
                             zip(training_sample_par, testing_sample_par)]

        # list_of_lists_with_kterms = p.map(par_learning, args_par_learning)
        list_of_lists_with_kterms = p.imap_unordered(par_learning, args_par_learning)

        # Remove empty lists
        list_of_lists_with_kterms = [sublist_kterms for sublist_kterms in list_of_lists_with_kterms if
                                     sublist_kterms != []]

        training_sample = list({kterm for sublist_kterms in list_of_lists_with_kterms for kterm in sublist_kterms})

        print('number of elements (kterms) in the training set after current iteration: ' + str(len(training_sample)))
        # At the end of each iteration, we obtain 'num_blocks' boolean functions with up to k0-terms per block.
        # Then, the total number of kterms is >= num_blocks * k0-terms.

        # The stopping condition is M >= total number of kterms.
        # By default, the number of kterms at the beginning of the first iteration is equal to the size of the
        # training sample.

    # Stop multiprocessing
    p.close()
    p.join()

    print('Composing partial results...')

    # Last iteration
    x = []
    y = []
    z = []

    # At the end of the concurrent part, we have 'num_blocks' boolean functions with up to k0 terms each one; M terms
    # in total.
    # Initialize the learning function for composing the partial results
    # T = Table(training_sample, cost_function='dist')
    T = Table(training_sample, cost_function='cost')
    # x, y, z = T.kdelearn(testing_sample=testing_sample, k=k, threshold=threshold, e=e, select='f',
    #                      print_on=False, print_latex=False)
    x, y = T.klearn(k=k, select='f', print_on=False, print_latex=False)

    # Length of the training_sample at the final iteration of the learning process
    # pad = len(initial_training_sample) - len(training_sample)
    # x = [xi + pad for xi in x]

    # pad = number of joins executed by the parallel learning process
    pad = len(initial_training_sample) - len(training_sample)
    x = [xi + pad for xi in x]

    # Result
    expected = sorted(DNF)
    result = sorted(T.terms)
    diff = (set(result) - set(expected)) | (set(expected) - set(result))

    print('Expected: {0}'.format(str(expected)))
    print('Result: {0}'.format(str(result)))
    print('Difference: {0}'.format(str(diff)))
    # print('Cost: {0}'.format(str(T.cost)))

    # print('x: {0}'.format(str(x)))
    # print('y: {0}'.format(str(y)))
    # print('z: {0}'.format(str(z)))
    # time.sleep(5)

    return x, y, z, T


if __name__ == '__main__':
    # ------------------------
    # Configuration parameters
    # ------------------------
    # Configuration of the parallel learning
    M = 200
    k0 = 50

    # Stopping conditions for learning process
    k = 10
    e = 0.1
    threshold = float('inf')

    # Sample size of the kterm-DNF
    minsample, maxsample, stepsample = (2000, 2010, 100)

    # DNF source (filenames)
    DNF_source = []
    # --------------------------------------------------

    # Command line arguments
    # sys.argv[]
    parser = argparse.ArgumentParser(description='Parallel learning of Boolean functions.')

    parser.add_argument('--m', metavar='M', dest='M', type=int, nargs='?', default=M,
                        help='max number of kterms that the sequential version of the learning process will process at'
                             ' the last stage (i.e., composition of partial results from parallel threads) '
                             '(default: {0})'.format(str(M)))
    parser.add_argument('--k0', metavar='k0', dest='k0', type=int, nargs='?', default=k0,
                        help='number of kterms computed by each thread per iteration (default: {0})'.format(str(k0)))

    # Stopping conditions
    parser.add_argument('--k', metavar='k', dest='k', type=int, nargs='?', default=k,
                        help='number of kterms of the kterm-DNF to learn (default: {0})'.format(str(k)))
    parser.add_argument('--e', metavar='e', dest='e', type=float, nargs='?', default=e,
                        help="percentage of false positives (p(h'|f)) tolerated for the inferred hypothesis function 'h' "
                             "(default: {0})".format(str(e)))
    parser.add_argument('--slope', metavar='slope', dest='slope', type=float, nargs='?', default=threshold,
                        help="max slope tolerated in the semantic size of the inferred hypothesis function 'h' when joining terms"
                             "(default: {0})".format(str(threshold)))

    # DNF source
    # parser.add_argument(metavar='DNF_source', dest='DNF_source', type=str, nargs=1, choices=['random', 'file'],
    parser.add_argument('--DNF_source', metavar='FILENAME', dest='DNF_source', type=str, nargs='*', default=DNF_source,
                        help="select the source file(s) of the DNF that will be used for learning (if None, generate random DNF)"
                             "(default: {0})".format(str(DNF_source)))

    # sample size
    parser.add_argument('--minsample', metavar='minsample', dest='minsample', type=int, nargs='?', default=minsample,
                        help='min number of samples taken from the DNF function'
                             '(default: {0})'.format(str(minsample)))
    parser.add_argument('--maxsample', metavar='maxsample', dest='maxsample', type=int, nargs='?', default=maxsample,
                        help='max number of samples taken from the DNF function'
                             '(default: {0})'.format(str(maxsample)))
    parser.add_argument('--stepsample', metavar='stepsample', dest='stepsample', type=int, nargs='?',
                        default=stepsample,
                        help='step between minsample and maxsample'
                             '(default: {0})'.format(str(stepsample)))

    args = parser.parse_args()
    # print args.accumulate(args.integers)

    # ------------------------
    # Configuration parameters
    # ------------------------
    # Configuration of the parallel learning
    M = args.M
    k0 = args.k0

    # Stopping conditions for learning process
    k = args.k
    e = args.e
    threshold = args.slope

    # Sample size of the kterm-DNF
    minsample, maxsample, stepsample = (args.minsample, args.maxsample, args.stepsample)

    # DNF source (filenames)
    DNF_source = args.DNF_source
    # --------------------------------------------------

    legend = []

    lines_hypothesis_size_numjoins = []
    lines_error_false_positives_numjoins = []

    lines_hypothesis_size_numterms = []
    lines_error_false_positives_numterms = []

    DNF_list = []

    #
    T = None
    #
    # Target kterm-DNF, i.e., function 'f' that we want to learn
    if len(DNF_source) == 0:
        print('Random')
        dim = 50
        num_stars = 10
        DNF_list += [random_DNF(k=k, dim=dim, min_order=num_stars, max_order=num_stars)]
    else:
        print('List ' + str(DNF_source))
        for DNF_filename in DNF_source:
            DNF_list += [read_DNF_from_file(DNF_filename)]
            print('DNF_list ' + str(DNF_list) + '\n')

    for DNF in DNF_list:
        # Sample S has a small portion of the size of the whole semantics of DNF
        print('Processing ' + str(DNF))
        S = sample_from_DNF_u(maxsample, *DNF)
        shuffle(S)

        print('Total sample size: ' + str(len(S)))

        # 1/3 of the sampling goes to training
        training_sample_common = S[: int(ceil(maxsample / 3.0))]

        # 2/3 of the sampling goes to testing
        testing_sample_common = S[1 + int(floor(maxsample / 3.0)):]

        for si in range(minsample, maxsample, stepsample):
            print('Current iteration takes {0} elements from the sample'.format(str(si)))

            # 1/3 of the sampling goes to training
            training_sample = training_sample_common[: int(ceil(si / 3.0))]

            # 2/3 of the sampling goes to testing
            testing_sample = testing_sample_common[: int(ceil(2.0 * si / 3.0))]

            print('Running parallel learning with training set of size {0} and testing set of size {1} elements'
                  .format(str(len(training_sample)), str(len(testing_sample))))

            x, y, z, T = parallel_learning(training_sample, testing_sample, M, k0, k, threshold, e)

            # x = [xi + num_proc * kterm for xi in x]

            lines_hypothesis_size_numjoins += [(x, y)]
            lines_error_false_positives_numjoins += [(x, z)]

            num_terms = [len(training_sample) - xi for xi in x]
            # num_terms = [int(ceil(len(S) / 3.0)) - xi for xi in x]
            lines_hypothesis_size_numterms += [(num_terms, y)]
            lines_error_false_positives_numterms += [(num_terms, z)]

            dim = max(len(DNF_i) for DNF_i in DNF)
            legend += ['kterm={0}, dim={1}, |S|={2}'.format(k, dim, len(training_sample))]
            # legend = ['kterm={0}, dim={1}, |S|={2}'.format(k, dim, int(ceil(len(S) / 3.0)))]

        # Generage graphics
        # plot_2D(x,y)
        plot_list_2D(lines_hypothesis_size_numjoins, legend=legend, xlabel='Number of joins', ylabel='|h| approx.',
                     title="Growth of hypothesis' size", xscale='linear', yscale='linear')  # yscale='log'

        # Do not uncomment the following 'plot_list_2D' because it will raise an error when printing 'z'.
        # This error is caused by the current parallel_learning configuration at the sequential step.
        # 'z' from '[(x, z)]' is empty because we have used klearn (not dlearn or kdelearn)
        # and false negatives are not calculated.

        # plot_list_2D(lines_error_false_positives_numjoins, legend=legend, xlabel='Number of joins', ylabel="p(h'|f)",
        #              title='Error growth', xscale='linear', yscale='linear')

        plot_list_2D(lines_hypothesis_size_numterms, legend=legend, xlabel='Number of terms', ylabel='|h| approx.',
                     title="Growth of hypothesis' size", xscale='linear', yscale='linear')  # yscale='log'

        # Do not uncomment the following 'plot_list_2D' because it will raise an error when printing 'z'.
        # This error is caused by the current parallel_learning configuration at the sequential step.
        # 'y' from '[(x, y)]' is empty because we have used klearn (not dlearn or kdelearn)
        # and false negatives are not calculated.

        # plot_list_2D(lines_error_false_positives_numterms, legend=legend, xlabel='Number of terms', ylabel="p(h'|f)",
        #              title='Error growth', xscale='linear', yscale='linear')

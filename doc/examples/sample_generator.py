import sys
import os
import argparse
import time
from random import shuffle
from math import ceil, floor
from multiprocessing import cpu_count, Pool

from BoolLearnLib.Table import *
from BoolLearnLib.Samples import *


def save_to_file(fname, DNF):
    # type: (str, list) -> None
    # fname = 'C:/path/numbers.txt'
    # Each line of the file contains a kterm of the DNF
    with open(fname, 'w') as f:
        for kterm in DNF:
            #f.writelines(kterm + os.linesep)
            f.write(kterm + '\n')


if __name__ == '__main__':
    # Default parameters
    mink, maxk, stepk = (10, 11, 1)
    mind, maxd, stepd = (50, 51, 1)
    minstar, maxstar, stepstar = (10, 11, 1)
    filename = 'out.txt'

    # Command line arguments
    # sys.argv[]
    parser = argparse.ArgumentParser(description='Sampling generator.')

    # number of kterms
    parser.add_argument('--mink', metavar='mink', dest='mink', type=int, nargs='?', default=mink,
                        help='min number of kterms of the DNF function'
                             '(default: {0})'.format(str(mink)))
    parser.add_argument('--maxk', metavar='maxk', dest='maxk', type=int, nargs='?', default=maxk,
                        help='max number of kterms of the DNF function'
                             '(default: {0})'.format(str(maxk)))
    parser.add_argument('--stepk', metavar='stepk', dest='stepk', type=int, nargs='?', default=stepk,
                        help='step between mink and maxk'
                             '(default: {0})'.format(str(stepk)))

    # dimension
    parser.add_argument('--mind', metavar='mind', dest='mind', type=int, nargs='?', default=mind,
                        help='min dimension of the DNF function'
                             '(default: {0})'.format(str(mind)))
    parser.add_argument('--maxd', metavar='maxd', dest='maxd', type=int, nargs='?', default=maxd,
                        help='max dimension of the DNF function'
                             '(default: {0})'.format(str(maxd)))
    parser.add_argument('--stepd', metavar='stepd', dest='stepd', type=int, nargs='?', default=stepd,
                        help='step between mind and maxd'
                             '(default: {0})'.format(str(stepd)))

    # number of stars
    parser.add_argument('--minstar', metavar='minstar', dest='minstar', type=int, nargs='?', default=minstar,
                        help='min number of stars of the DNF function'
                             '(default: {0})'.format(str(minstar)))
    parser.add_argument('--maxstar', metavar='maxstar', dest='maxstar', type=int, nargs='?', default=maxstar,
                        help='max number of stars of the DNF function'
                             '(default: {0})'.format(str(maxstar)))
    parser.add_argument('--stepstar', metavar='stepstar', dest='stepstar', type=int, nargs='?', default=stepstar,
                        help='step between minstar and maxstar'
                             '(default: {0})'.format(str(stepstar)))

    # filename
    parser.add_argument('--filename', metavar='FILENAME', dest='filename', type=str, nargs='?', default=filename,
                        help='each sample will be stored in a file named "filename[i]"'
                             '(default: {0})'.format(str(filename)))

    args = parser.parse_args()
    # print args.accumulate(args.integers)

    mink, maxk, stepk = (args.mink, args.maxk, args.stepk)
    mind, maxd, stepd = (args.mind, args.maxd, args.stepd)
    minstar, maxstar, stepstar = (args.minstar, args.maxstar, args.stepstar)
    filename = args.filename

    name_and_extension = os.path.splitext(filename)

    for kterm in range(mink, maxk, stepk):
        for dim in range(mind, maxd, stepd):
            for num_stars in range(minstar, maxstar, stepstar):
                fname = '{0}_{1}_{2}_{3}{4}'.format(name_and_extension[0], kterm, dim, num_stars, name_and_extension[1])

                DNF = random_DNF(k=kterm, dim=dim, min_order=num_stars, max_order=num_stars)
                save_to_file(fname, DNF)

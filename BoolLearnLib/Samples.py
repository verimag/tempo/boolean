# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:23:36 2017
last update on Wed 6 Jun 18

@author: elefther, nrequeno
"""

from math import ceil
import random

import numpy as np

from BoolLearnLib.Terms import Term, cube, semantics, sem_size_appr, inclusion, inclusion_dnf

# ==============================================================================
# ==============================================================================
# # random functions
# ==============================================================================
# ==============================================================================


# ==============================================================================
# random objects
# ==============================================================================

# def random_vectorB(dim = 5):
#    """From a Boolean Cube od given dimension dim (default = 5) return a sample of size (default = 8)
#    """
#    return np.random.choice(cube(dim))

# def random_term(k,n):
#    """n is the number of variables in total, and k is the number of care variables in the term."""
#    assert k < n # the size of the sub-cube (term) is smaller than the dimension B^n
#    stars = random.sample(range(n),k)
#    term = ''
#    for i in range(n):
#        if i in stars: term += '*'
#        else: term += random.choice(['0','1'])
#    return term

# def random_term(k,n):
#    """n is the number of variables in total, and k is the number of care variables in the term."""
#    assert k < n # the size of the sub-cube (term) is smaller than the dimension B^n
#    a = [random.choice(['0','1']) for i in range(n-k)]+['*']*k
#    np.random.shuffle(a)
#    return ''.join(a)

def random_term_old(k, n):
    """n is the number of variables in total, and k is the number of care variables in the term."""
    assert k <= n, 'the size of the sub-cube (term) is greater than the dimension B^n'
    k0 = random.randint(0, n - k)
    a = ['0'] * k0 + ['1'] * (n - k - k0) + ['*'] * k
    np.random.shuffle(a)
    return ''.join(a)


def random_term(k, n):
    random_list = []
    for i in range(n - k):
        # random.choice(['0', '1'])
        random_list.append(str(random.randint(0, 1)))

    a = random_list + ['*'] * k
    np.random.shuffle(a)
    return ''.join(a)


def random_DNF(k, dim, min_order=0, max_order=None):
    """returns a set of k terms of dimension dim."""
    assert min_order >= 0
    if max_order is None: max_order = dim
    assert min_order <= max_order
    assert max_order <= dim

    DNF = set()
    while len(DNF) < k:
        DNF.add(random_term(random.randint(min_order, max_order), dim))
    return sorted(DNF)


# ==============================================================================
# random samples using semantics
# ==============================================================================

def random_sample(dim=5, size=8):
    """From a Boolean Cube od given dimension dim (default = 5) return a sample of size (default = 8)
    """
    return sorted(np.random.choice(cube(dim), size=size))


def sample_from_rectangle(rectangle, size):
    """returns a sample of given size that is chosen uniformly from the rectangle (term)"""
    assert size <= 2 ** rectangle.count('*')
    return sorted(random.sample(semantics(rectangle), size))


# def sample_from_DNF(dim = 10, sample_size = 8, k = 4):
#    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
#    """
#    assert k <= dim
#    DNF = random_DNF(k, dim, min_order = 2, max_order = 4)
#    sem = set([j for i in DNF for j in semantics(i)])
#    return random.sample(sem,sample_size)

def sample_from_DNF(sample_size=5, *terms):
    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
    """
    if terms == ():
        print('sample chosen from a random DNF')
        terms = random_DNF(4, 10, min_order=2, max_order=4)
    sem = set([j for i in terms for j in semantics(i)])
    return sorted(random.sample(sem, sample_size))


# ==============================================================================
# random samples WITHOUT using the semantics
# ==============================================================================

def random_vector(n):
    """returns a random vector of size (#variables) n"""
    if n <= 50:
        return random_vector_short(n)
    else:
        return random_vector_long(n)


def random_vector_short(n):
    """returns a random vector of size (#variables) n
    NOTE: faster from dimensions up to ~50, for longer vectors use random_vector_long"""
    num_ones = random.randrange(n)
    vector = '1' * num_ones + '0' * (n - num_ones)
    return ''.join(random.sample(vector, len(vector)))


def random_vector_long(n):
    """returns a random vector of size (#variables) n
    NOTE: faster from dimensions ~larger than 50, for shorter vectors use random_vector_short"""
    return ''.join(np.random.choice(['0', '1'], n))


def random_sample_u(dim=5, size=8):
    """From a Boolean Cube od given dimension dim (default = 5) return a sample of size (default = 8)
    """
    assert size <= 2 ** dim
    sample = set()
    while len(sample) < size:
        sample.add(''.join([random.choice(['0', '1']) for i in range(dim)]))
    return sorted(sample)


# ==============================================================================

def sample_from_rectangle_u(rectangle, size):
    """returns a sample of given size that is chosen uniformly from the rectangle (term)"""
    assert size <= 2 ** rectangle.count('*')
    sample = set()
    while len(sample) < size:
        sample.add(''.join([i if i in ['0', '1'] else random.choice(['0', '1']) for i in rectangle]))
    return sorted(sample)


def sample_from_DNF_u(sample_size=5, *terms):
    """From a random DNF, determined from some given parameters, return a sample of size (default = 8)
    """
    assert sample_size <= sem_size_appr(*terms), \
        'Error, the sample size is {0} and sem_size is {1}'.format(str(sample_size), str(sem_size_appr(*terms)))
    sample = set()
    sizes = [2 ** (i.count('*')) for i in terms]
    # prob = [float(i) / sum(sizes) for i in sizes]
    res_sum = sum(sizes)
    prob = [float(i) / res_sum for i in sizes]
    while len(sample) < sample_size:
        # select one rectangle
        rectangle = np.random.choice(terms, 1, p=prob)[0]  # change this to weighted probability instead of uniform
        # select uniformly one vector in it
        a = ''.join([i if i in ['0', '1'] else random.choice(['0', '1']) for i in rectangle])
        # rejection probability
        k = 1. / [inclusion(Term(a), Term(i)) for i in terms].count(True)
        # with prob 1/k update sample with new vector
        # otherwise continue with next iteration
        if bool(np.random.choice([True, False], 1, p=[k, 1 - k])):
            sample.add(a)

    return sorted(sample)


def sample_from_DNFc_u(sample_size=5, *terms):
    """From a random DNF, return a sample of size (default = 8) that belongs to its complement
    """
    n = len(terms[0])
    sample = set()
    while len(sample) < sample_size:
        vector = ''.join(np.random.choice(['0', '1'], n))
        if inclusion_dnf(vector, *terms):
            continue
        sample.add(vector)
    return sorted(sample)


# =============================================================================
# DNF with simple overlap
# =============================================================================
def DNF_simple_overlap(n=60, d=30, shift=3):
    # n = length of the word
    # d = number of '0's in the word
    terms = set()
    interval = int(ceil((n - d) / shift + 1))
    for i in range(interval):
        s = ''
        for j in range(n):
            if (j < shift * i + d and j > shift * i - 1):
                s += '0'
            else:
                s += '*'
        terms.add(s)
    return terms


def generate_simple_overlap(n=60, d=30, shift=3):
    terms = []
    interval = int(n / shift)
    for i in range(interval):
        s = ['*'] * n
        for j in range(d):
            s[(i * shift + j) % n] = '0'
        terms.append(''.join(map(str, s)))
    return terms


def DNF_simple_overlap_list(n=60, d=30, shift=3):
    terms = []
    interval = int(ceil((n - d) / shift + 1))
    for i in range(interval):
        s = ''
        for j in range(n):
            if (j < shift * i + d and j > shift * i - 1):
                s += '0'
            else:
                s += '*'
        terms.append(s)
    return terms


# ==============================================================================
# fixed samples
# ==============================================================================

def fixed_sample():
    return ['10100', '10101', '10110', '10111', '11100', '11101', '11110', '11111']


def fixed_sample2():
    return ['111000', '011110', '001010', '010110', '101011', '101100', '001111', '001000', '000000']


def fixed_sample3():
    return ['111000', '001010', '101011', '101100', '001111', '01*110', '00*000']


def fixed_sample4():
    return ['111000', '011110', '001010']
